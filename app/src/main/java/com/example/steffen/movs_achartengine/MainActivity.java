package com.example.steffen.movs_achartengine;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    LinearLayout chartLyt;
    Spinner spinner;
    Random r = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chartLyt = (LinearLayout) findViewById(R.id.chart);
        spinner = (Spinner) findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getItemAtPosition(position).toString();

                if (selected.equals("XY")){
                    createChar(3, "XY Example", "Line");
                } else if (selected.equals("Bar")){
                    createChar(3, "XY Example", "Bar", BarChart.Type.DEFAULT);
                } else if (selected.equals("Pie")){
                    createPieChart(5);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }

    public void createChar(int count, String title, String type){
        this.createChar(count, title, type, null);
    }


    public void createChar(int count, String title, String type, BarChart.Type chartType){
        chartLyt.removeAllViews();

        XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();

        //Full chart renderer
        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
        // We want to avoid black border
        // transparent margins
        mRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));

        mRenderer.setYAxisMin(0);
        mRenderer.setShowGrid(true); // we show the grid

        mRenderer.setChartTitle(title);
        mRenderer.setXTitle("My X-Title");
        mRenderer.setYTitle("My Y-Title");

        mRenderer.setAxisTitleTextSize(30);
        mRenderer.setLegendTextSize(30);
        mRenderer.setLabelsTextSize(20);
        mRenderer.setChartTitleTextSize(40);

        mRenderer.setGridColor(Color.GRAY);
        mRenderer.setLabelsColor(Color.BLACK);

        mRenderer.setMargins(new int[]{50, 50, 200, 22});
        mRenderer.setFitLegend(true);

        //mRenderer.setZoomEnabled(true, false);

        mRenderer.setPanEnabled(true, false); //Draggable

        mRenderer.setClickEnabled(false);


        //Create count XYSeries
        for (int l = 0; l < count; l++){
            //Create new XY-Series
            XYSeries xySeries = new XYSeries("Series " + l);
            for (int i = 0; i < 35; i++){
                xySeries.add(i, r.nextInt(21)); // Add random Y-Values
            }
            mDataset.addSeries(xySeries);

            //New renderer
            XYSeriesRenderer xyRenderer = new XYSeriesRenderer();
            xyRenderer.setLineWidth(2); //in pixels


            xyRenderer.setColor(getRandomColor());

            //Include low and max value
            xyRenderer.setDisplayBoundingPoints(true);
            //Add point markers
            xyRenderer.setPointStyle(PointStyle.CIRCLE);
            xyRenderer.setPointStrokeWidth(3);

            mRenderer.addSeriesRenderer(xyRenderer);
        }

        //Create View
        GraphicalView chartView = null;
        if (type.equals("Line")){
            chartView = ChartFactory.getLineChartView(this, mDataset, mRenderer);
        } else if (type.equals("Bar")) {
            chartView = ChartFactory.getBarChartView(this, mDataset, mRenderer, chartType);
        }
        chartLyt.addView(chartView,0);
    }


    public void createPieChart(int count) {
        chartLyt.removeAllViews();

        DefaultRenderer mRenderer = new DefaultRenderer();
        // set the start angle for the first slice in the pie chart
        mRenderer.setStartAngle(180);
        // display values on the pie slices
        mRenderer.setDisplayValues(true);

        mRenderer.setLegendTextSize(30);
        mRenderer.setLabelsTextSize(20);
        mRenderer.setChartTitleTextSize(40);
        mRenderer.setChartTitle("Pie Test");

        mRenderer.setLabelsColor(Color.BLACK);

        //Create count XYSeries
        CategorySeries mSeries = new CategorySeries("");

        for (int l = 0; l < count; l++) {
            mSeries.add("Series " + (mSeries.getItemCount() + 1), r.nextInt(21));

            SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
            renderer.setColor(getRandomColor());
            mRenderer.addSeriesRenderer(renderer);

        }


        GraphicalView chartView = ChartFactory.getPieChartView(this, mSeries, mRenderer);
        chartLyt.addView(chartView,0);
    }


    public int getRandomColor(){
        int red = (int) (Math.random() * 245);
        int green = (int) (Math.random() * 245);
        int blue = (int) (Math.random() * 245);
        return (Color.rgb(red, green, blue));
    }

}
